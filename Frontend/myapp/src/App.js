import './App.css';
import Home from './Pages/Home';
import About from './Pages/About';
import Services from './Pages/Services';



function App() {
  return (
    <div >
     <Home />
     <About />
     <Services />
    </div>
  );
}

export default App;
